<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <form action="/welcome" method="POST">
      @csrf
      <h1> Buat Account Baru! </h1>
      <h3> Sign Up Form </h3>
      <p> First Name: </p>
      <input type="text" name="firstname">
      <p> Last Name: </p>
      <input type="text" name="lastname">
      <p> Gender: </p>
      <input type="radio" name="gender"> Male <br>
      <input type="radio" name="gender"> Female <br>
      <input type="radio" name="gender"> Others
      <p> Nationality: </p>
      <select>
        <option>Indonesian</option>
        <option>Singaporean</option>
        <option>Malaysian</option>
        <option>Australian</option>
      </select>
      <p> Language Spoken: </p>
      <input type="checkbox"> Bahasa Indonesia <br>
      <input type="checkbox"> English <br>
      <input type="checkbox"> Others
      <p> Bio: </p>
      <textarea rows="7" cols="25"></textarea>
      <br>
      <input type="submit" value="Sign Up">
    </form>
  </body>
</html>
